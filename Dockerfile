FROM ubuntu:18.04 as prod

ENV VERSION=0.41

RUN apt-get update -y && \
    apt-get install -y \
    wget \
    gnupg \
    gnupg2 \
    software-properties-common

RUN wget -O - https://facebook.github.io/mcrouter/debrepo/xenial/PUBLIC.KEY | apt-key add && \
    add-apt-repository 'deb https://facebook.github.io/mcrouter/debrepo/bionic bionic contrib' && \
    apt-get update -y && \
    apt-get install -y mcrouter

ENTRYPOINT [ "mcrouter" ]